﻿using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Astar {
    public partial class Node : UserControl {
        Rectangle[] arms;
        public static SolidColorBrush ActiveColor = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF49AFCD"));
        public static SolidColorBrush EndColor = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF13EA31"));
        public static SolidColorBrush StartColor = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFFF0000"));
        public static SolidColorBrush BlockColor = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFFFFFFF"));
        public static SolidColorBrush PathColor = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFFFFB00"));


        public int[] pos;
        public double globalGoal = int.MaxValue;
        public double localGoal = int.MaxValue;
        public bool isTested = false;
        public bool isBlock = false;
        public Node parent = null;

        public Node(int x, int y)
        {
            InitializeComponent();
            pos = new int[] { x, y };
            arms = new Rectangle[] { topArm, rightArm, bottomArm, leftArm };
            
        }
        public void ColorArm(int i, bool show, SolidColorBrush color = null, bool removeArm = false) {
            color = color ?? ActiveColor;
            bool[] arr = new bool[arms.Length];
            arr[i] = show;
            ColorArms(arr, color);
            if (removeArm) {
                arms[i] = new Rectangle();
            }
        }

        public void ColorArms(bool[] arr, SolidColorBrush color = null) {
            color = color ?? ActiveColor;
            for(int i = 0; i < arms.Length; i++) {
                if (arr[i]) {
                    arms[i].Fill = color;
                    if (color == PathColor)
                        Panel.SetZIndex(arms[i], 2);
                    else
                        Panel.SetZIndex(arms[i], 0);
                }
            }
        }

        public void ToggleBlock() {
            isBlock = !isBlock;
            if (isBlock)
                SetColor(BlockColor);
            else
                SetColor(ActiveColor);
        }
        public Node SetColor(SolidColorBrush color) {
            square.Fill = color;
            if(square.Fill == ActiveColor) {
                Panel.SetZIndex(square, 0);
            }
            else {
                Panel.SetZIndex(square, 1);
            }
            return this;
        }
        public void Reset() {
            globalGoal = int.MaxValue;
            localGoal = int.MaxValue;
            isTested = false;
            parent = null;
            foreach (Rectangle arm in arms) {
                if (arm.Fill == PathColor) {
                    arm.Fill = ActiveColor;
                    Panel.SetZIndex(arm, 0);
                }
            }
        }
       
    }
}
