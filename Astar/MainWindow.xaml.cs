﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Astar {

    public partial class MainWindow : Window
    {
        Node start;
        Node end;
        Node[,] nodeArr;
        CancellationTokenSource tokenSource;
        public MainWindow()
        {
            InitializeComponent();
            MakeGrid((int)slValue.Value);
            //MakeBlock(nodeArr[10, 10]);

        }

        private void GridSlider(object sender, RoutedPropertyChangedEventArgs<double> e) {
            MakeGrid((int)slValue.Value);
        }
        private void Click(object sender, MouseButtonEventArgs e) {
            Node node = (Node)sender;
            if (sender == start || sender == end)
                return;
            if (node.isBlock && !Keyboard.IsKeyDown(Key.LeftCtrl))
                MakeBlock(node);
            switch (e.ChangedButton) {
                case MouseButton.Left:
                    if (Keyboard.IsKeyDown(Key.LeftCtrl)) {
                        MakeBlock(node);
                        break;
                    }
                    node.SetColor(Node.EndColor);
                    if (end == null) {
                        end = node;
                    }
                    else {
                        end.SetColor(Node.ActiveColor);
                        end = node;
                    }
                    break;
                case MouseButton.Right:
                    
                    node.SetColor(Node.StartColor);
                    if (start == null) {
                        start = node;
                    }
                    else {
                        start.SetColor(Node.ActiveColor);
                        start = node;
                    }
                    break;
            }
            if(start != null && end != null) {
                CalculatePath();
            }
        }

        private void MakeGrid(int n) {
            nodeGrid.ColumnDefinitions.Clear();
            nodeGrid.RowDefinitions.Clear();
            nodeGrid.Children.Clear();
            start = null;
            end = null;
            nodeArr = new Node[n, n];
            for (int i = 0; i < n; i++) {
                nodeGrid.ColumnDefinitions.Add(new ColumnDefinition());
                nodeGrid.RowDefinitions.Add(new RowDefinition());
                for (int j = 0; j < n; j++) {
                    nodeArr[i, j] = new Node(i, j);
                    nodeArr[i, j].MouseUp += Click;
                    if (i == 0) {
                        nodeArr[i, j].ColorArm(0, true, Brushes.Transparent, true);
                    }
                    else if (i == n - 1) {
                        nodeArr[i, j].ColorArm(2, true, Brushes.Transparent, true);
                    }
                    if(j == 0) {
                        nodeArr[i, j].ColorArm(3, true, Brushes.Transparent, true);
                    }
                    else if(j == n - 1) {
                        nodeArr[i, j].ColorArm(1, true, Brushes.Transparent, true);
                    }
                    nodeGrid.Children.Add(nodeArr[i, j]);
                    Grid.SetRow(nodeArr[i, j], i);
                    Grid.SetColumn(nodeArr[i, j], j);
                }
            }
        }
        private void MakeBlock(Node n) {
            n.ToggleBlock();
            List<Node> neighbors = GetNeighbors(n, true);
            foreach (Node node in neighbors) {
                SolidColorBrush color = n.isBlock || node.isBlock ? Brushes.Transparent : Node.ActiveColor;
                node.ColorArm(GetDirection(node, n), true, color);
                n.ColorArm(GetDirection(n, node), true, color);
            }
        }
        private void ResetNodes() {
            for(int i = 0; i < nodeArr.GetLength(0); i++) {
                for (int j = 0; j < nodeArr.GetLength(0); j++) {
                    nodeArr[i, j].Reset();
                }
            }
        }
        private double GetDistance(Node n1, Node n2) {
            return GetDistance(n1.pos, n2.pos);
        }
        private double GetDistance(int[] pos1, int[] pos2) {
            int square(int x) => x * x;
            return Math.Sqrt(square(pos2[0] - pos1[0]) + square(pos2[1] - pos1[1]));
        }

        private List<Node> GetNeighbors(Node node, bool any = false) {
            List<Node> neighbors = new List<Node>();
            int x = node.pos[0];
            int y = node.pos[1];

            int[][] dirr = new int[][] {
                new int[] {-1, 0},
                new int[] {0, 1},
                new int[] {1, 0},
                new int[] {0, -1}
            };
            bool WithinBounds(int xpos, int ypos) => !(xpos < 0 || ypos < 0) && !(xpos >= nodeArr.GetLength(0) || ypos >= nodeArr.GetLength(0));

            foreach (int[] dir in dirr) {
                int nX = x + dir[0];
                int nY = y + dir[1];
                if(WithinBounds(nX, nY)) {
                    if(any || !nodeArr[nX, nY].isBlock) {
                        neighbors.Add(nodeArr[nX, nY]);
                    }
                }
            }
            return neighbors;
        }
        private int GetDirection(Node n1, Node n2) {
            int x1 = n1.pos[0];
            int y1 = n1.pos[1];
            int x2 = n2.pos[0];
            int y2 = n2.pos[1];

            if(x2 < x1 && y2 == y1)
                return 0;
            if (x2 == x1 && y2 > y1)
                return 1;
            if (x2 > x1 && y2 == y1)
                return 2;
            if (x2 == x1 && y2 < y1)
                return 3;

            return -1;
        }
        private void MakeConnection(Node n1, Node n2) {
            n1.ColorArm(GetDirection(n1, n2), true, Node.PathColor);
            n2.ColorArm(GetDirection(n2, n1), true, Node.PathColor);
        }
        private void CalculatePath() {
            ResetNodes();
            List<Node> NodesToTest = new List<Node> {
                start
            };
            start.globalGoal = GetDistance(start, end);
            start.localGoal = 0;
            Node currNode;
            while (NodesToTest.Count > 0) {
                currNode = NodesToTest[0];

                List<Node> neighbors = GetNeighbors(currNode);
                foreach (Node neighbor in neighbors) {
                    if(!neighbor.isTested && !NodesToTest.Contains(neighbor)) {
                        if(!(neighbor == end))
                            NodesToTest.Add(neighbor);
                    }
                    double sum = currNode.localGoal + GetDistance(currNode, neighbor);
                    if (sum < neighbor.localGoal) {
                        neighbor.parent = currNode;
                        neighbor.localGoal = sum;
                        neighbor.globalGoal = sum + GetDistance(neighbor, end);
                    }
                }
                currNode.isTested = true;
                NodesToTest.Remove(currNode);
                NodesToTest = NodesToTest.OrderBy(o => o.globalGoal).ToList();
            }
            if (tokenSource != null)
                tokenSource.Cancel();
            tokenSource = new CancellationTokenSource();
            Draw(end, tokenSource.Token);
            
        }
        public async void Draw(Node end, CancellationToken token) {
            while (!token.IsCancellationRequested && end.parent != null) {
                MakeConnection(end, end.parent);
                end = end.parent;
                await Task.Delay((int)slValue2.Value);
            }
            tokenSource = null;
        }
    }

    

}
